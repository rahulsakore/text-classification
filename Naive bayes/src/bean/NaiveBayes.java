package bean;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Group 7
 * 
 */
public class NaiveBayes {

	Map<String, String> nbMap = new HashMap<String, String>();
	List<String> positiveList;
	List<String> negativeList;
	List<String> neutralList;

	List<String> termList;
	List<int[]> countList;
	List<double[]> probList;
	int positiveTotal, negativeTotal, neutralTotal;
	int positiveCount, negativeCount, neutralCount;

	public NaiveBayes() throws IOException {
		//TODO File read
		//TODO Put data into Map
		
//		nbMap.put("I love Honda", "positive");
//		nbMap.put("Ford is good and powerful", "positive");
//		nbMap.put("Volkswagen is economical", "positive");
//		nbMap.put("I hate Mazda", "negative");
//		nbMap.put("I hate Maggie", "negative");
//		nbMap.put("Used General Motor is difficult to sell", "negative");
//		nbMap.put("BMW has bad brand image in this there", "negative");
//		nbMap.put("Lots of new models are introduced this year", "neutral");
//		nbMap.put("My neighbor's car is under repairment", "neutral");
//		nbMap.put("We won the tournament", "positive");
		
		
		BufferedReader br = new BufferedReader(
				new FileReader("R:/Semester 2 Spring 2016/KPT/Group7_Project/Twitter_updated.csv"));

		String line = null;

		while ((line = br.readLine()) != null) {
			String[] values = line.split(",",2);
			//System.out.println(values);
			//values[1] = values[1].replaceAll("[\t\"',:.?;!/#%@&_-]+", "");
			nbMap.put(values[1], values[0]);
		}
		System.out.println("Printing it----------");

		
		//		for (Map.Entry<String, String> entry : nbMap.entrySet()) {
//
//			String key = entry.getKey();
//			String type = entry.getValue();
//			System.out.println(type + "------" + key);
//		}
		
	}

	/**
	 * 
	 * @param bean
	 * @return
	 */
	public String acceptTwieet(NaiveBayesBeans bean) {
		String inputText = null;
		String outString = "Please enter tweet string.";
		System.out.println("Inside NaiveBayes...Start");

		System.out.println("Inside NaiveBayes...input" + bean.getInputText());
		if (bean.getInputText() != null) {
			trainNB();
			inputText = bean.getInputText();
			int outCount = classifyNB(inputText);
			System.out.println(inputText + " " + outCount);
			if (outCount == 1) {
				outString = "Positive tweet";
			} else if (outCount == -1) {
				outString = "Negative tweet";
			} else if (outCount == 0) {
				outString = "Neutral tweet";
			}

		}
		System.out.println("Inside NaiveBayes...End");

		return outString;
	}

	private void trainNB() {
		positiveList = new ArrayList<String>();
		negativeList = new ArrayList<String>();
		neutralList = new ArrayList<String>();

		termList = new ArrayList<String>();
		countList = new ArrayList<int[]>();
		probList = new ArrayList<double[]>();

		for (Map.Entry<String, String> entry : nbMap.entrySet()) {

			String key = entry.getKey();
			String type = entry.getValue();
			System.out.println(type + "-" + key);

			switch (type) {
			case "positive":
				positiveList.add(key);
				break;
			case "negative":
				negativeList.add(key);
				break;
			case "neutral":
				neutralList.add(key);
				break;
			default:
				throw new IllegalArgumentException("Invalid value: " + type);

			}
			
			
			PreProcess pre = new PreProcess();
			String[] processedTweet, temp1, temp2;
			temp1 = (String[]) (pre.process1(key)).clone();
			temp2 = (String[]) (pre.process2(temp1)).clone();
			processedTweet = (String[]) (pre.process3(temp2)).clone();

			//String[] processedTweet = key.split(" ");
			System.out.println("Process------ "+ Arrays.toString(processedTweet));
			for (String word : processedTweet) {
				int index = termList.indexOf(word);

				if (index == -1) {
					termList.add(word);
					int[] wordCount = { 0, 0, 0 };
					
					String theType = type;
					//System.out.println("type is:--"+ theType+"--");
					if ("positive".equalsIgnoreCase(theType)) {
						wordCount[0] = 1;
						positiveTotal++;
					} else if ("negative".equalsIgnoreCase(theType)) {
						wordCount[1] = 1;
						negativeTotal++;
					} else if ("neutral".equalsIgnoreCase(theType)) {
						wordCount[2] = 1;
						neutralTotal++;
					}
					countList.add(wordCount);
				} else {
					int[] wordCountNew = countList.get(index);
					String type1 = type;
					//System.out.println("type is: " + type1);
					if (type1 == "positive") {
						wordCountNew[0]++;
						positiveTotal++;
					} else if (type1 == "negative") {
						wordCountNew[1]++;
						negativeTotal++;
					} else if (type1 == "neutral") {
						wordCountNew[2]++;
						neutralTotal++;
					}
					countList.set(index, wordCountNew);
				}

			}

		}
		System.out.println("neutral list:" + neutralTotal);
		System.out.println("positive list:" + positiveTotal);
		System.out.println("negative list:" + negativeTotal);

		// Calculate NaiveBayes probability
		for (int i = 0; i < termList.size(); i++) {
			double[] prob = new double[3];
			int j = (countList.get(i))[0];
			prob[0] = 1.0 * ((countList.get(i))[0] + 1)
					/ (positiveTotal + termList.size());
			prob[1] = 1.0 * ((countList.get(i))[1] + 1)
					/ (positiveTotal + termList.size());
			prob[2] = 1.0 * ((countList.get(i))[2] + 1)
					/ (positiveTotal + termList.size());
			probList.add(prob);
		}
		System.out.println("-----------------"+probList.size());
	}

	/**
	 * NaiveBayes classifying: use model to classify a tweet
	 * 
	 * @param input
	 *            : tweet (String)
	 * @return: 1 if positive, -1 if negative, 0 if neutral
	 */
	private int classifyNB(String tweet) {

		positiveCount = positiveList.size();
		negativeCount = negativeList.size();
		neutralCount = neutralList.size();

		double positiveProb = 1.0 * positiveCount / nbMap.size();
		double negativeProb = 1.0 * negativeCount / nbMap.size();
		double neutralProb = 1.0 * neutralCount / nbMap.size();
		
		PreProcess pre = new PreProcess();
		String[] processedTweet, temp1, temp2;
		temp1 = (String[]) (pre.process1(tweet)).clone();
		temp2 = (String[]) (pre.process2(temp1)).clone();
		processedTweet = (String[]) (pre.process3(temp2)).clone();
		
		System.out.println("Procccccc "+ Arrays.toString(processedTweet));
		
		//String[] processedTweet = tweet.split(" ");
		for (String word : processedTweet) {
			int index = termList.indexOf(word);

			// Only consider word in termList
			if (index >= 0) {
				positiveProb *= (probList.get(index))[0];
				negativeProb *= (probList.get(index))[1];
				neutralProb *= (probList.get(index))[2];
			}
		}

		// Normalization
		double positive = 1.0 * positiveProb
				/ (positiveProb + negativeProb + neutralProb);
		double negative = 1.0 * negativeProb
				/ (positiveProb + negativeProb + neutralProb);
		double neutral = 1.0 * neutralProb
				/ (positiveProb + negativeProb + neutralProb);

		if (positive >= negative & positive >= neutral)
			return 1;
		else if (negative >= positive & negative >= neutral)
			return -1;
		return 0;
	}

}

//positive-Im looking forward to the day that Googles self driving car powers the newly merged Uber and Zipcar
//Process------ [Im, looking, forward, to, the, day, that, Googles, self, driving, car, powers, the, newly, merged, Uber, and, Zipcar]
//positive-The idea is to challenge traditional automotive design The Geneva Auto Show + Teslas very sexy driverless car gtgt httptcoHYOYLX297o
//Process------ [The, idea, is, to, challenge, traditional, automotive, design, The, Geneva, Auto, Show, +, Teslas, very, sexy, driverless, car, gtgt, httptcoHYOYLX297o]
//neutral-Lmfaoooooo RT ch000ch my self driving google car wont pull over for this cop and is dumping drugs out the window please help
