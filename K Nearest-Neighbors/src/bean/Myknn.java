package bean;

/**
 * @author Group 7
 * 
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Myknn {
	Map<String, String> knnMap = new HashMap<String, String>();

	ArrayList<String> tweetList = new ArrayList<String>();
	ArrayList<String> typeList = new ArrayList<String>();
	ArrayList<String> testTweetList = new ArrayList<String>();
	ArrayList<String> testTypeList = new ArrayList<String>();
	ArrayList<String> predictList = new ArrayList<String>();

	public ArrayList<String> termList = new ArrayList<String>();
	public ArrayList<int[]> countList = new ArrayList<int[]>();
	public ArrayList<Double> igList = new ArrayList<Double>();

	public ArrayList<String> vTermList = new ArrayList<String>();
	public ArrayList<double[]> vectorMatrix = new ArrayList<double[]>();
	public int positiveCount, negativeCount, neutralCount;
	public double igThreshold;
	public int k;

	public Myknn() throws IOException {
		igThreshold = 0.5;
		k = 1;
		
//      tweetList.add("I don't love Honda:]");
//      tweetList.add("Ford is endurable and powerful.");
//      tweetList.add("Volkswagen is economical:). ");
//      tweetList.add("I hate Mazda:[");
//      tweetList.add("Used General Motor is difficult to sell.");
//      tweetList.add("BMW has bad brand image in this area, here and therer:(");
//      tweetList.add("Lots of new models are introduced this yea");
//      tweetList.add("My neighbor's car is under repairment");
//      typeList.add("negative");
//      typeList.add("positive");
//      typeList.add("positive");
//      typeList.add("negative");
//      typeList.add("negative");
//      typeList.add("negative");
//      typeList.add("neutral");
//      typeList.add("neutral");
      
//		
		
		
		BufferedReader br = new BufferedReader(new FileReader(
				"/Users/Shreyas/Downloads/Twitter_updated.csv"));

		String line = null;

		while ((line = br.readLine()) != null) {
			String[] values = line.split(",", 2);
			// System.out.println(values);
			//values[1] = values[1].replaceAll("[\t\"',:.?;!/#%@&_-]+", "");
			knnMap.put(values[1], values[0]);
		}
		for (Map.Entry<String, String> entry : knnMap.entrySet()) {

			String key = entry.getKey();
			tweetList.add(key);
			String type = entry.getValue();
			typeList.add(type);
			//System.out.println(type + "------" + key);
		}
		
		

//		String test2 = "my car has good mileage)";
//		String test1 = "This model is hard to sell due to bad bad bad brand image:(";
//		testTweetList.add(test1);
//		testTweetList.add(test2);
//		testTypeList.add("positive");
//		testTypeList.add("negative");

//		createCountList();
//		createIgList();
//		createVectorMatrix();

//		System.out.println("\n" + "Classification result:");
//		System.out.println(test1 + " " + classifyKNN(test1));
//		System.out.println(test2 + " " + classifyKNN(test2));
//		System.out.println("\n" + "Classification accuracy is: " + classifyAll());

	}
	
	public String acceptTweet(KnnBeans bean) {
		String inputText = null;
		String outString = "Please enter tweet string.";
		System.out.println("Inside NaiveBayes...Start");

		System.out.println("Inside NaiveBayes...input" + bean.getInputText());
		if (bean.getInputText() != null) {
			//trainNB();
			createCountList();
			createIgList();
			createVectorMatrix();

			inputText = bean.getInputText();
			outString = classifyKNN(inputText);
			

		}
		System.out.println("Inside KNN...End");

		return outString;
	}

	public void createCountList() {

		for (int i = 0; i < tweetList.size(); i++) {
			PreProcess pre = new PreProcess();
			String[] processedTweet, temp1, temp2;
			temp1 = (String[]) (pre.process1(tweetList.get(i))).clone();
			temp2 = (String[]) (pre.process2(temp1)).clone();
			processedTweet = (String[]) (pre.process3(temp2)).clone();

			String type = typeList.get(i);
			System.out.println("Processed------> "+ Arrays.toString(processedTweet));
			for (String word : processedTweet) {
				int index = termList.indexOf(word);

				if (index == -1) {
					termList.add(word);
					int[] wordCount = new int[tweetList.size()];
					wordCount[i] = 1;
					countList.add(wordCount);
				} else {
					(countList.get(index))[i]++;
				}
			}
		}
	}

	public void createIgList() {
		int positiveCount = Collections.frequency(typeList, "positive");
		int negativeCount = Collections.frequency(typeList, "negative");
		int neutralCount = Collections.frequency(typeList, "neutral");

		double gini = 1.0 - Math.pow(2, positiveCount / typeList.size())
				- Math.pow(2, negativeCount / typeList.size())
				- Math.pow(2, neutralCount / typeList.size());

		for (int i = 0; i < termList.size(); i++) {
			int[] igCount = { 0, 0, 0, 0, 0, 0 };

			for (int j = 0; j < tweetList.size(); j++) {

				if ("positive".equalsIgnoreCase(typeList.get(j))
						//typeList.get(j) == "positive" 
						& (countList.get(i))[j] > 0)
					igCount[0]++;
				else if ("negative".equalsIgnoreCase(typeList.get(j)) 
					//(typeList.get(j) == "negative"
						& (countList.get(i))[j] > 0)
					igCount[1]++;
				else if ("neutral".equalsIgnoreCase(typeList.get(j)) 
					//(typeList.get(j) == "neutral"
						& (countList.get(i))[j] > 0)
					igCount[2]++;
				else if ("positive".equalsIgnoreCase(typeList.get(j))
					//(typeList.get(j) == "positive"
						& (countList.get(i))[j] == 0)
					igCount[3]++;
				else if ("negative".equalsIgnoreCase(typeList.get(j))
					//(typeList.get(j) == "negative"
						& (countList.get(i))[j] == 0)
					igCount[4]++;
				else
					igCount[5]++;
			}

			double gini1 = 1.0
					- Math.pow(2, 1.0 * (igCount[0] + 0.01)
							/ (igCount[0] + igCount[1] + igCount[2] + 0.1))
					- Math.pow(2, 1.0 * (igCount[1] + 0.01)
							/ (igCount[0] + igCount[1] + igCount[2] + 0.1))
					- Math.pow(2, 1.0 * (igCount[2] + 0.01)
							/ (igCount[0] + igCount[1] + igCount[2] + 0.1));
			double gini2 = 1.0
					- Math.pow(2, 1.0 * (igCount[3] + 0.01)
							/ (igCount[3] + igCount[4] + igCount[5] + 0.1))
					- Math.pow(2, 1.0 * (igCount[4] + 0.01)
							/ (igCount[3] + igCount[4] + igCount[5] + 0.1))
					- Math.pow(2, 1.0 * (igCount[5] + 0.01)
							/ (igCount[3] + igCount[4] + igCount[5] + 0.1));
			double giniNew = 1.0 * (igCount[0] + igCount[1] + igCount[2])
					/ tweetList.size() * gini1 + 1.0
					* (igCount[3] + igCount[4] + igCount[5]) / tweetList.size()
					* gini2;
			double ig = gini - giniNew;

			igList.add(new Double(ig));
		}
		/**
		 * System.out.println("Informatioin gain:"); for(int
		 * i=0;i<termList.size();i++)
		 * System.out.println(termList.get(i)+": "+igList.get(i));
		 **/
	}

	public void createVectorMatrix() {

		for (int i = 0; i < termList.size(); i++) {
			if (igList.get(i) > igThreshold) {
				vTermList.add(termList.get(i));
				double[] temp = new double[tweetList.size()];
				for (int j = 0; j < tweetList.size(); j++) {
					temp[j] = 1.0 * (countList.get(i))[j];
				}
				vectorMatrix.add(temp);
			}
		}

		double sumSquare = 0.0;

		for (int i = 0; i < tweetList.size(); i++) {
			for (int j = 0; j < vTermList.size(); j++) {
				sumSquare += Math.pow(2, (vectorMatrix.get(j))[i]);
			}
			sumSquare = Math.sqrt(sumSquare);
			for (int j = 0; j < vTermList.size(); j++) {
				(vectorMatrix.get(j))[i] /= sumSquare;
			}
		}
		/**
		 * System.out.println("Vector Matrix:"); for(int
		 * i=0;i<vTermList.size();i++) {
		 * System.out.print(vTermList.get(i)+": "); for(int
		 * j=0;j<tweetList.size();j++)
		 * System.out.print(" "+vectorMatrix.get(i)[j]); System.out.print("\n");
		 * }
		 **/
	}
	
	/**
	 * KNN classifying: use model to classify a tweet
	 * @param tweet
	 * @return: 1 if positive, -1 if negative, 0 if neutral
	 */

	public String classifyKNN(String tweet) {

		int[] queryVector = new int[vTermList.size()];
		double[] cosineList = new double[tweetList.size()];
		double[] cosineRank = new double[tweetList.size()];
		HashMap<Double, Integer> hm = new HashMap<Double, Integer>();

		String[] processedTweet, temp1, temp2;
		PreProcess pre = new PreProcess();
		temp1 = (String[]) (pre.process1(tweet)).clone();
		temp2 = (String[]) (pre.process2(temp1)).clone();
		processedTweet = (String[]) (pre.process3(temp2)).clone();
		System.out.println("processed : "+ Arrays.toString(processedTweet));
		for (String word : processedTweet) {
			int index = vTermList.indexOf(word);

			// Only consider word in termList
			if (index >= 0)
				queryVector[index]++;
		}

		for (int i = 0; i < tweetList.size(); i++) {
			double cosine = 0.0;
			for (int j = 0; j < vTermList.size(); j++) {
				cosine += 1.0 * queryVector[j]
						* (1.0 * (vectorMatrix.get(j))[i]);
			}

			cosineList[i] = cosine;
			cosineRank[i] = cosine;
			hm.put(cosine, i);
		}
		/**
		 * System.out.println("Cosine Distance:"); for(int
		 * i=0;i<tweetList.size();i++) System.out.print(cosineList[i]+" ");
		 * System.out.println("");
		 **/
		int positive = 0;
		int negative = 0;
		int neutral = 0;

		// Rank cosine similarity and find k nearest neighbor
		for (int i = 0; i < k; i++) {
			for (int j = i + 1; j < tweetList.size(); j++) {
				if (cosineRank[i] < cosineRank[j]) {
					double temp = cosineRank[i];
					cosineRank[i] = cosineRank[j];
					cosineRank[j] = temp;
				}
			}
			String type = typeList.get(hm.get(cosineRank[i]));
			if  ("positive".equalsIgnoreCase(type))
				//(type == "positive")
				positive++;
			else if ("negative".equalsIgnoreCase(type))
				//(type == "negative")
				negative++;
			else if ("neutral".equalsIgnoreCase(type))
				neutral++;
		}
		// System.out.println("Count pos/neg/neu: "+positive+" "+negative+" "+neutral);

		if (positive >= negative & positive >= neutral)
			return "positive";
		else if (negative >= positive & negative >= neutral)
			return "negative";
		return "neutral";
	}

	public double classifyAll() {
		for (int i = 0; i < testTweetList.size(); i++) {
			predictList.add(classifyKNN(testTweetList.get(i)));
		}

		// Calculate NaiveBayes Classification accuracy
		int correctPrediction = 0;
		for (int i = 0; i < predictList.size(); i++) {
			if (predictList.get(i).equals(testTypeList.get(i)))
				correctPrediction++;
		}
		double accuracy = 1.0 * correctPrediction / predictList.size();
		return accuracy;
	}
	
//	public static void main(String[] args) throws IOException{
//		Myknn mk = new Myknn();
//	}

}
