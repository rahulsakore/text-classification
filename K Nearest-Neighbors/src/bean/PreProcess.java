package bean;

   import java.io.*;
   import java.util.*;


    public class PreProcess {
      public static File stopwordFile = new File("/Users/Shreyas/Downloads/stopwords.txt");
      public ArrayList<String> stopList;
      String tweet1, tweet2, tweet3;
      String[] tweet11, tweet12, tweet13;
   
       PreProcess() {
         createStopList();
      }
   
       public static void main(String[] args) {		
         PreProcess test = new PreProcess();
         test.tweet1 = "Nonsense -&gt; RT @sarahcuda: For self-driving cars, we don't trust tech companies more than auto makers?:( http://t.co/2DH0X8nLD5";
         test.tweet2 = "Guess what we were driving next to on the I-80? A driverless car. Nice! (@ I-80) [pic]: http://t.co/9xbkLkw1";
         test.tweet3 = "Google's Self-Driving Car Can't Navigate Heavy Rain Or 99% of Roads http://t.co/jE9HVw8R99";
            
         test.tweet11 = (String[])(test.process1(test.tweet1)).clone(); 
         for(String word: test.tweet11) {
            System.out.print(word+" | ");
         }
         System.out.print("\n"); 
      
         test.tweet12 = (String[])(test.process2(test.tweet11)).clone();
         for(String word: test.tweet12) {
            System.out.print(word+" | ");
         }
         System.out.print("\n"); 
         
         test.tweet13 = (String[])(test.process3(test.tweet12)).clone();
         for(String word: test.tweet13) {
            System.out.print(word+" | ");
         }
         System.out.print("\n"); 
      }
   
   /**
       public void read(String Name)
      {
         String line;
         try {
            BufferedReader br = new BufferedReader(new FileReader(Name));
            line = br.readLine();
            String nextline = br.readLine();
            while (nextline != null) {
               line = line+nextline;
               nextline=br.readLine();
            }
            br.close();
            arr = line.toCharArray();	  
         }
             catch (Exception e) {
               if(e!=null) System.out.println(e.getMessage());
            }
      }
   **/
   
   
   //Transform emoticons into word-like tokens, also remove 'http://...'
       public String Emoticon(String raw){
         String result = raw;
         for(int i=0; i<result.length()-1; i++){
            char x,y;
            x=result.charAt(i);
            y=result.charAt(i+1);
            if(x==':'){
               if(y==')'|y==']'|y=='D'|y=='3')
                  result = result.substring(0,i)+" #PE "+result.substring(i+2,result.length());
               else if(y=='('|y=='c'|y=='[')
                  result = result.substring(0,i)+" #NE "+result.substring(i+2,result.length());
            }
            if(x=='X'){
               if(y=='D')
                  result = result.substring(0,i)+" #PE "+result.substring(i+2,result.length());
            }
         }
       
      	//Remove 'http://...' in tweets
         for(int i=result.length()-3; i>=0; i--){
            char x,y,z;
            x=result.charAt(i);
            y=result.charAt(i+1);
            z=result.charAt(i+2);
            if(x=='h' & y=='t' & z=='t'){
               result = result.substring(0,i);
               return result;
            }
         }
         return result;
      }
   
   
   //Insert space before punctuation
       public String Punctuation(String a){
         String temp = a;
         for(int i=0; i<temp.length(); i++){
            char x;
            x=temp.charAt(i);
            if(x==','|x=='.'|x==';'|x=='['|x==']'|x=='('|x==')'|x=='{'|x=='}'|x=='+'| x=='*'|x=='/'|x=='?'|x=='!') {
               temp = temp.substring(0,i)+" "+temp.substring(i,a.length());
               i++;
            }
         }
         return temp;
      }
      
   
   //Stem words in tweets
       public String[] process1(String tweet){
         String processedTweet = Emoticon(tweet);
         processedTweet = Punctuation(processedTweet);
      
         String[] splitedTweet = processedTweet.split(" ");
         
         for(int i=0; i<splitedTweet.length; i++) {
            String word = splitedTweet[i].toLowerCase();
            Stemmer aStemmer = new Stemmer();
            aStemmer.add(word.toCharArray(), word.length());
            aStemmer.stem();
            String stemmedWord = aStemmer.toString();
            splitedTweet[i] = stemmedWord;
         }
         return splitedTweet;
      }
   
   //Process negation
       public String[] process2(String[] tweet){
         String[] processedTweet = new String[tweet.length];
         for(int i=0; i<tweet.length; i++) {
            processedTweet[i]=tweet[i];
         }
         boolean neg = false;
                     
         for(int i=0; i<processedTweet.length; i++) {
            String word = processedTweet[i];
            if (neg==false) {
               if (word.equals("not")|word.equals("isn't")|word.equals("aren't")|word.equals("wasn't")|word.equals("weren't")|word.equals("hasn't")
               |word.equals("haven't")|word.equals("hadn't")|word.equals("won't")|word.equals("can't")|word.equals("couldn't")|word.equals("don't")
               |word.equals("doesn't")|word.equals("didn't")|word.equals("ain't")|word.equals("shouldn't")) {
                  neg = true;
                  continue;
               }
            }
            else if (neg==true) {
               if (word.equals(",")|word.equals(".")|word.equals(";")) {
                  neg = false;
               }
               else if(word.equals("#ne")|word.equals("#pe")) {
                  continue;
               }
               else {
                  processedTweet[i] = word+"_N";
               }   	
            }
            //System.out.println(processedTweet[i]+" " +neg);
         }
         return processedTweet;
      }
   
       void createStopList(){
         stopList = new ArrayList<String>();
           
         try {
            Scanner scan = new Scanner(stopwordFile);
            while(scan.hasNextLine()) {
               stopList.add(scan.nextLine());
            }
         }
             catch(Exception e) {
               System.out.println(e.getMessage());
            }
         
      }
   
       public int searchStopWord(String key) {
         int lo = 0;
         int hi = stopList.size() -1;
         while(lo <= hi) {
            int mid = lo + (hi-lo)/2;
            int result = key.compareTo(stopList.get(mid));
            if(result < 0) hi = mid - 1;
            else if(result > 0) lo = mid + 1;
            else 
               return mid;
         }
         return -1;
      }
      
   	//Remove stopwords and meaningless words like '@...' or '/...' 
       public String[] process3(String[] raw)	{
      
         ArrayList<String> temp = new ArrayList<String>();
         for(int index=0;index<raw.length;index++) {
            String token = raw[index];
            String token2 = token;
            int i = token.length();
            if(i>=2) {
               if(token.charAt(i-2)=='_' & token.charAt(i-1)=='N') {
                  token2 = token.substring(0,i-2);
                  //System.out.println(token+" "+token.charAt(i-2));
               }
            }
            if (searchStopWord(token2) != -1) 
               continue;
            else if (token2.equals("")|token2.equals(",")|token2.equals(".")|token2.equals(";")|token2.equals("!")|token2.equals("?")) 
               continue;
            else if (token.charAt(0)=='@' | token.charAt(0)=='/' | token.charAt(0)=='-' | token.charAt(0)=='(' | token.charAt(0)==')' | token.charAt(0)=='[' | token.charAt(0)==']')
               continue;
            else{
               temp.add(token);
            }
         }
         
         String[] result = new String[temp.size()];
         for(int i=0;i<temp.size();i++) {
            result[i]=temp.get(i);
         }
         return result;
      }
   
   }