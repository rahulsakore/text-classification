Twitter Sentimental Analysis using K-Nearest Neighbors and Naive Bayes Algorithm.

The data used to test demo is  8 "manipulated tweets" with polarity given, and it is incorporated in the .java files. However, with minor change of the codes, the algorithms 
can be modified to process real processed dataset as input.

The algorithms assumes that the input is processed data. Raw tweets should be tokenized, stemmed and with widely-accepted stop words removed. Specially, emoticons 
should be extracted as tokens, and feature extractioin should be conducted by adding NOT_ to every word between negation and following punctuation.

The data structure of input dataset are ArrayList<String[]> to store tokens, and ArrayList<String> to store polarity.  

1 NaiveBayes
1.1 Count term frequency in each tweets from training dataset, and store data in ArrayList<int[]>
1.2 Calculate posterior probability of each term given polarity, and store data in ArrayList<double[]>
1.3 Calculate posterior probability of each tweets from test dataset, and store data in ArrayList<int[]>
1.4 Classify tweets

2 KNN
2.0 User need to define k and threshold of info gain
2.1 Count term frequency in each tweets from training dataset and store data in ArrayList<int[]>
2.2 Calculate information gain of each term and store data in ArrayList<double[]>
2.3 Remove terms of which the information gain is less than threshold
2.4 Calculate the vector of each tweet, normalize vectors and store data in ArrayList<double[]>
2.5 Calculate cosine similarity of each tweets from test dataset and store data in ArrayList<Double>
2.6 Pick k nearest neighbors and classify tweets
